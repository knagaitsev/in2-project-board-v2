var request = require("request");

module.exports.Forms = function() {

}

module.exports.Forms.prototype = {
    getData: function() {
        return new Promise(function(fulfill, reject) {
            request('https://api.typeform.com/v1/form/QM1bXb?key=f7c20ccfda29d7e50dea0cd8b1a21bb6442c5ba2', function (error, response, body) {
                console.log('error:', error);
                console.log('statusCode:', response && response.statusCode);
                console.log('body:', body);
                fulfill(body);
            });
        });
    }
};
