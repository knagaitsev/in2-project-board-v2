process.on('unhandledRejection', up => { throw up });

var functions = require('firebase-functions');
var admin = require('firebase-admin');
var serviceAccount = require("./cred/key.json");

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://in2-project-board.firebaseio.com"
});
var fs = require("fs");
const $ = require('cheerio');

var forms = require("./forms");

var GoogleSpreadsheet = require('google-spreadsheet');
var doc = new GoogleSpreadsheet('12FOLfVvFc7tRqWe9kCVVVOoq0Rp7bv2Zv2n6KxzhY4M');

var fs = require("fs");

var request = require("request");

exports.projectBoard = functions.https.onRequest(function(req,res) {
    var html = fs.readFileSync("index.html", "utf8");
    doc.getInfo(function(err, info) {
        var sheet = info.worksheets[0];
        sheet.getCells({"min-row": 2}, function(err,cells) {
            var projects = getData(cells);
            var previewHtml = getPreviewHtml(projects);
            var csvData = fs.readFileSync("UN.csv", "utf8").split("\n");
            console.log(csvData);
            var mainHtml = getMainHtml(projects, csvData);
            html = html.replace(getMatchString("previewHtml"), previewHtml);
            html = html.replace(getMatchString("mainHtml"), mainHtml);
            res.status(200).send(html);
            /*
			getDataFromFirebase().then(function(projects) {
				var previewHtml = getPreviewHtml(projects);
	            var csvData = fs.readFileSync("UN.csv", "utf8").split("\n");
	            console.log(csvData);
	            var mainHtml = getMainHtml(projects, csvData);
	            html = html.replace(getMatchString("previewHtml"), previewHtml);
	            html = html.replace(getMatchString("mainHtml"), mainHtml);
	            res.status(200).send(html);
            });
            */
        });
        //res.status(200).send(sheet.title);
    });
});
/*
exports.admin = functions.https.onRequest(function(req,res) {
    var f = new forms.Forms();
    f.getData().then(function(body) {
        res.status(200).send(body);
    });
});*/

function getData(cells) {
    var rawProjectData = [];
    var currentRow = null;
    for (var i = 0 ; i < cells.length ; i++) {
        var cell = cells[i];
        var row = parseInt(cell.row) - 2;
        var col = parseInt(cell.col) - 1;
        if (row != currentRow) {
            rawProjectData.push([]);
            currentRow = row;
        }
        rawProjectData[row][col] = cell.value;
    }

    var projects = [];
    for (var i = 0 ; i < rawProjectData.length ; i++) {
        var project = {};
        var data = rawProjectData[i];
        var keys = ["title", "type", "show", "shortDesc", "longDesc", "goals",
                    "leaderData", "driverData", "members", "leaderEmailData", "driverEmailData", "open"];
        for (var j = 0 ; j < data.length ; j++) {
            if (j < keys.length) {
                project[keys[j]] = data[j];
            }
        }
        if (project.show == "yes") {
            project.show = true;
        }
        else {
            project.show = false;
        }

        if (project.open == "yes") {
            project.open = true;
        }
        else {
            project.open = false;
        }

        project.leaders = parsePersonalData(project.leaderData, project.leaderEmailData);
        project.leaders.forEach(function(leader, index) {
            var name = leader.name.split(" ");
            var letter = name[0].split("")[0] + ".";
            leader.name = letter;
            for (var i = 1 ; i < name.length ; i++) {
                leader.name += " " + name[i];
            }
            leader.email = "";
        });
        project.drivers = parsePersonalData(project.driverData, project.driverEmailData);

        if (project.goals) {
            var goalData = project.goals.split(",");
            var goals = [];
            for (var j = 0 ; j < goalData.length ; j++) {
                var goal = goalData[j];
                goal = goal.trim();
                var data = goal.split(".");
                var number = data[0];
                var target = false;
                if (data.length == 2) {
                    target = data[1];
                }
                var found = false;
                for (var k = 0 ; k < goals.length ; k++) {
                    if (goals[k].number == number) {
                        if (target && goals[k].targets.indexOf(target) == -1) {
                            goals[k].targets.push(target);
                        }
                        found = true;
                    }
                }
                if (!found) {
                    var obj = {
                        number: number,
                        targets: []
                    };
                    if (target) {
                        obj.targets.push(target);
                    }
                    goals.push(obj);
                }
            }
            project.goals = goals;
        }

        projects.push(project);
    }
    return projects;
}

function getDataFromFirebase() {
	return new Promise(function(fulfill, reject) {
		admin.database().ref("projects").once("value").then(function(snap) {
			var projectData = snap.val();
			var projects = [];
			Object.keys(projectData).forEach(function(key, index) {
				projects.push(projectData[key]);
			});
			fulfill(projects);
		});
	});
}

function getPreviewHtml(projects) {
    var html = "";
    projects.forEach(function(project,index) {
        if (project.show) {
            var container = $("<p></p>");
            var elem = $("<div></div>").appendTo(container);
            elem.attr("class", project.type);
            elem.attr("data-index", index);
            if (project.goals) {
                var goalList = getGoalNumbers(project.goals);
                elem.attr("data-sdg", goalList.join(","));
            }
            $("<h1>" + project.title + "</h1>").appendTo(elem);
            if (project.shortDesc) {
                $("<p>" + project.shortDesc + "</p>").appendTo(elem);
            }

            if (project.open) {
                $("<span class='is-open tt'>OPEN<span class='tt-text'>Taking new members</span></span>").prependTo(elem);
            }
            else {
                $("<span class='is-closed tt'>CLOSED<span class='tt-text'>Not taking new members</span></span>").prependTo(elem);
            }
            html += container.html();
        }
    });

    return html;
}

function getMainHtml(projects, csvData) {
    var html = "";
    projects.forEach(function(project,index) {
        if (project.show) {
            var container = $("<p></p>");
            var elem = $("<div></div>").appendTo(container);
            elem.attr("data-index", index);
            $("<h1>" + project.title + "</h1>").appendTo(elem);
            if (project.longDesc) {
                $("<p>" + project.longDesc + "</p>").appendTo(elem);
            }
            else if (project.shortDesc) {
                $("<p>" + project.shortDesc + "</p>").appendTo(elem);
            }

            if (project.leaders && project.leaders.length > 0) {
                $("<h4>"+getIconHtml("check-square-o")+" Led By</h4>").appendTo(elem);

				project.leaders.forEach(function(leader,index) {
	                $("<p>" + leader.name + getEmailHtml(leader.email) + "</p>").appendTo(elem);
	            });
            }
            if (project.goals && project.goals.length > 0) {

                $("<h4>"+getIconHtml("globe")+" UN Sustainable Development Goals</h4>").appendTo(elem);
                var txt = "";
                for (var j = 0 ; j < project.goals.length ; j++) {
                    var num = project.goals[j].number;
                    num = (num.length == 1 ? "0" : "") + num;
                    txt += `<img src="/asset/sdg/E_SDG goals_icons-individual-rgb-${num}.png">`;
                }
                $("<div>" + txt + "</div>").appendTo(elem);

                $("<h4>"+getIconHtml("crosshairs")+" UN SDG Targets</h4>").appendTo(elem);
                for (var j = 0 ; j < project.goals.length ; j++) {
                    var targets = project.goals[j].targets;
					if (targets && targets.length > 0) {
						for (var k = 0 ; k < targets.length ; k++) {
	                        var t = targets[k];
	                        var id = project.goals[j].number + "." + t;
	                        for (var l = 0 ; l < csvData.length ; l++) {
	                            var arr = csvData[l].split("\t");
	                            if (arr[0] == id) {
	                                $("<p>" + id + ": " + arr[2] + "</p>").appendTo(elem);
	                            }
	                        }
	                    }
					}
                }
            }

            if (project.drivers && project.drivers.length > 0) {
                $("<h4>"+getIconHtml("trophy")+" Driver/Champion</h4>").appendTo(elem);

				project.drivers.forEach(function(driver,index) {
	                $("<p>" + driver.name + getEmailHtml(driver.email) + "</p>").appendTo(elem);
	            });
            }

            if (project.members) {
                $("<h4>"+getIconHtml("users")+" Members/Collaborators</h4>").appendTo(elem);
                $("<p>" + project.members + "</p>").appendTo(elem);
            }
            html += container.html();
        }
    });

    return html;
}

function getMatchString(key) {
    return "<!--${" + key + "}-->";
}

function getIconHtml(key) {
    return '<i class="fa fa-' + key + '" aria-hidden="true"></i>';
}

function getEmailHtml(email) {
    if (email && email.length > 0) {
        return `&nbsp<a id="blueBox" class="email-box" href="mailto:${email}"><i class="fa fa-envelope-o" aria-hidden="true"></i>
          <span id="proj-driver-email">${email}</span>
        </a>`;
    }

    return "";
}

function parsePersonalData(names,emails) {
    var people = [];
    var nameData = null;
    var emailData = null;
    if (names) {
        var nameData = names.split(",").map(function(item) {
            return item.trim();
        });
    }
    if (emails) {
        var emailData = emails.replace(/\s+/g, '').split(",");
    }
    if (nameData) {
        for (var i = 0 ; i < nameData.length ; i++) {
            var person = {};
            person.name = nameData[i];
            if (emailData && i < emailData.length &&
            emailData[i].length > 0 && emailData[i] != "none") {
                person.email = emailData[i];
            }
            people.push(person);
        }
    }

    return people;
}

function getGoalNumbers(data) {
    var nums = [];
    for (var i = 0 ; i < data.length ; i++) {
        nums.push(data[i].number);
    }
    return nums;
}

function flattenGoals(data) {
    var goals = [];
    for (var i = 0 ; i < data.length ; i++) {
        for (var j = 0 ; j < data[i].targets.length ; j++) {
            goals.push(data[i].number + "." + data[i].targets[j]);
        }
    }
    return goals;
}

//make a login form where an admin then can make a request for the typeform forms with the bearer header
//make something to read in typeform submissions (new page) - new firebase request
//make that thing editable and submittable to firebase via post, where it will then write it on the google forms

const typeform = require('typeform')('f7c20ccfda29d7e50dea0cd8b1a21bb6442c5ba2');


//options presented to user when they select a response:
//delete response, update project with this response, create new project with this response
//Other options:
//edit project directly, delete (archive) project
exports.getForm = functions.https.onRequest(function(req, res) {
	authorize(req.headers["authorization"]).then(function() {
		admin.database().ref("archived_response_tokens").once("value").then(function(snap) {
			getForm().then(function(form) {
				res.status(200).send(JSON.stringify(form));
			});
		});
	}).catch(function() {
		res.status(400).send("Bad Request");
	});
});
/*
exports.getProjects = functions.https.onRequest(function(req, res) {
	admin.database().ref("projects").once("value").then(function(snap) {
		var projectData = snap.val();
		var projects = [];
		Object.keys(projectData).forEach(function(key, index) {
			projects.push(projectData[key]);
		});
		res.status(200).send(JSON.stringify(projects));
	});
});
*/

/*
exports.editProject = functions.https.onRequest(function(req, res) {

});

exports.addProject = functions.https.onRequest(function(req, res) {

});
*/

function exportToFirebaseDatabase() {
    doc.getInfo(function(err, info) {
        var sheet = info.worksheets[0];
        sheet.getCells({"min-row": 2}, function(err,cells) {
            var projects = getData(cells);
			admin.database().ref("projects").set(null).then(function() {
				for (var i = 0 ; i < projects.length ; i++) {
					Object.keys(projects[i]).forEach(function(key, index) {
						if (projects[i][key] === undefined) {
							projects[i][key] = null;
						}
					});

					admin.database().ref("projects").push(projects[i]);
				}
			});
        });
    });
}

//exportToFirebaseDatabase();

function getForm() {

    return new Promise(function(fulfill, reject) {
        const typeform_UID = "QM1bXb";
        const typeform_API_key = 'f7c20ccfda29d7e50dea0cd8b1a21bb6442c5ba2';
        request(`https://api.typeform.com/v1/form/${typeform_UID}?key=${typeform_API_key}`, function(req, res, body) {
            var form = JSON.parse(body);
            for (var i = 0 ; i < form.responses.length ; i++) {
                //console.log(form.responses[i]);
            }
            fulfill(form);
        });
    });
}

function authorize(authHeader) {
	return new Promise(function(fulfill, reject) {
		console.log(authHeader);
		var authArr = authHeader.split(" ");
		var idToken = authArr[authArr.length - 1];
		admin.auth().verifyIdToken(idToken).then(function(decodedToken) {
			var uid = decodedToken.uid;
			admin.database().ref("users/" + uid + "/type").once("value").then(function(snap) {
				if(snap.val() == "admin") {
					fulfill();
				}
				else {
					reject();
				}
			}).catch(function() {
				reject();
			});
		}).catch(function(error) {
			reject();
		});
	});
}

//exportToFirebaseDatabase();
//getForm();
