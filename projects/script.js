var Excel = require('exceljs');
var workbook = new Excel.Workbook();
var fs = require("fs");
workbook.csv.readFile("projects.csv").then(function(worksheet) {
	console.log(worksheet);
	var projects = {};
	projects.list = [];
	worksheet.eachRow(function(row, rowNumber) {
		if (rowNumber > 1) {
			var proj = {};
			var projData = [];
			row.eachCell(function(cell, cellNumber) {
				projData.push(cell.value);
			});
			proj.type = "project";
			proj.status = ["closed"];
			proj.title = projData[1];
			proj.shortDescription = projData[2];
			proj.longDescription = projData[3];
			proj.grandChallengeCategory = projData[7];
			proj.members = projData[6].split(", ");
			proj.ledBy = getPeopleObjs(projData[4]);
			proj.projectDrivers = getPeopleObjs(projData[5]);

			projects.list.push(proj);
		}
	});
	fs.writeFile("../public/projects2.js", "var projects = " + JSON.stringify(projects, null, '\t') + ";", function(err) {
		if (err) {

		}
		else {
			console.log("written");
		}
	});
});

function getPeopleObjs(data) {
	var rawPeople = data.split(", ");
	var formatPeople = [];
	rawPeople.forEach(function(person, index) {
		var personData = person.split(" (");
		var personObj = {};
		if (personData.length === 2) {
			personObj.email = personData[1].replace(")", "");
		}
		personObj.name = personData[0];
		formatPeople.push(personObj);
	});

	return formatPeople;
}