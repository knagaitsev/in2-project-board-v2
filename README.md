Welcome to the IMSA Impact App. The Impact App is a webpage that displays current IMSA courses, research, IN2 Projects, programs, and upcoming events. Front-end JavaScript makes the website dynamic. The website is hosted on Firebase, where the App's database and server reside.

### Getting Started with Development

To start developing the IMSA Impact App, you will need the following tools on your machine:

- Node.js
- npm (Node Package Manager)
- In your Command Prompt do: npm install -g firebase-tools
- Git
- You will need permissions for the existing Firebase project on your Google account
- Navigate to a directory to place your project in on Command Prompt (by doing "cd <directory path>")
- In the Command Prompt, do "git clone <url of this repo>"
- Then, do "firebase use" and select the correct Firebase project
- Now, you can do "firebase serve" to test locally

### Known Issues and Potential Flaws

These are issues that are not urgent, but could affect the App in the future.

- Database security and robustness could be better (making sure user-supplied info is valid)

### Road map and future development

This is where the project is going in the future:

- Building an admin console on the website where admin users can edit projects and handle form submissions
- Creating permissions so that Project owners can edit their own project information
- Built-in forms on the website, not Typeform forms
- Making the Firebase database more robust with backups and history
- Refining the existing codebase to be readable and better structured


### How Data is Passed Around

On the actual app, the server sends the client data like this:

- The client makes the https request which is sent to Firebase Functions
- The server fetches the data from Firebase database (previously it sent a request to a Google Spreadsheet containing data)
- The data is formatted as a list of projects, courses, etc. Each of these items is an object with basic text properties like *title*, *short description*, and *long description*. It also contains a few lists of "person" objects, which are objects with *name* and *email* properties. Each item also contains a list of "goal" objects. Each "goal" object has a *number* property (which is 1-15) and also an optional *targets* property, which is a list of numbers that specify the SDG targets.
- This data is formatted into HTML elements using the npm package cheerio.
- Titles and short descriptions are used to create card elements on the scrolling left side of the app
- All data is placed into hidden elements on the large card on the right side of the screen
- All of this html data is simply added to a static html file with some variables where these particular dynamic elements should be located
- This html is then sent back to the client
- When the client clicks on a card on the left of the screen, jQuery is used to identify which project ID was clicked. Then it identifies this project within the hidden elements of the large card, and makes that particular element visible
- When the user clicks a new card, the currently visible element is hidden, and a new element is brought into view
- The filters aspect of the webpage works by associating a filtering function with a button. For example, when an SDG goal filter is clicked, a function is added to a list of functions that are currently being used for filtering. These functions are all sent a jQuery html element. The function identifies what project this is using the html element. If the filter includes this element, the function returns true. If not, false is returned. Some filters toggle other filters off, like the "courses", "research", "IN2 Projects", etc. filters. Those filter conditions must be met for a project to be displayed, and I tend to refer to them as "and" filters the way the boolean logic works. Other filters work in a group, and I refer to these as "or" filters. If the conditions of any of the filters in a group is met, the element is displayed.

The admin console works like this:

- There is first a login form presented to users, which uses email and password
- This is handled using a template form modal, along with firebase auth
- Once the user is logged in (and has admin permissions on the firebase database), calls are made to both the firebase database and the project/course submission Typeform API
- A list of projects from the database is loaded onto the front of this admin console, and below it, a list of Typeform submissions
- The projects can be clicked to view. There is also a button to Edit them. They are stored in the same object format as described above. jQuery is used to make modal "edit" forms, with dynamic elements that help to ensure robustness of the database
- Typeform submissions can be clicked to archive, create a new project with, or edit an existing project with (by merging)
- The typeform submission objects are converted into objects that are compatible with the firebase database directly after the Typeform API is called